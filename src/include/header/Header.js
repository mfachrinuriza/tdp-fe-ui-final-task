import '../../assets/css/style.css'
import './Header.css'
// assets image
import banner from '../../assets/img/architect.jpg';

const Header = () => {
    return(
        <div>
            <header>
                <img className='img-banner' src={banner} width={1590} alt="Architecture" />
                <h1>
                    <b>EDTS</b><span>Architects</span>
                </h1>
            </header>
        </div>
    )
}

export default Header