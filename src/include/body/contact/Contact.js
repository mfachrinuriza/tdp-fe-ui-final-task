import './Contact.css'
import '../../../assets/css/style.css'

const Contact = () =>{
    return(
        <div>
            {/* Contact Section */}
            <div id="contact">
                <h3>Contact</h3>
                <hr/>
                <p>Lets get in touch and talk about your next project.</p>
                {/***  
                add your element form here 
                ***/}
                <form>
                    <div className='form-control'>
                        <input type="text" id="name" class="input-box" placeholder="Name" required></input>
                    </div>
                    <div className='form-control'>
                        <input type="email" id="email" class="input-box" placeholder="Email" required></input>
                    </div>
                    <div className='form-control'>
                        <input type="text" id="subject" class="input-box" placeholder="Subject" required></input>
                    </div>
                    <div className='form-control'>
                        <input type="text" id="comment" class="input-box" placeholder="Comment"></input>
                    </div>
                    <div className='form-control'>
                        <input className='button' type="submit" value="SEND MESSAGE"></input>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Contact