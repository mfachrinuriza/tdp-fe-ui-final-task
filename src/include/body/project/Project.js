import './Project.css'
import '../../../assets/css/style.css'

import house2 from '../../../assets/img/house2.jpg';
import house3 from '../../../assets/img/house3.jpg';
import house4 from '../../../assets/img/house4.jpg';
import house5 from '../../../assets/img/house5.jpg';

const Project = () => {
    return(
        <div>
            {/* Page content */}
            <div class="title-photo">
                {/* Project Section */}
                <div id="projects">
                    <h3>Projects</h3>
                    <hr/>
                </div>
            </div>
            <div class="grid-project">
                <div class="photo1">
                    <div className="black-box">Summer House</div>
                    <img src={house2} alt="House" />
                </div>
                <div class="photo2">
                    <div className="black-box">Brick House</div>
                    <img src={house3} alt="House" />
                </div>  
                <div class="photo3">
                    <div className="black-box">Renovated</div>
                    <img src={house4} alt="House" />
                </div>
                <div class="photo4">
                    <div className="black-box">Barn House</div>
                    <img src={house5} alt="House" />
                </div>
                
                <div class="photo5">
                    <div className="black-box">Summer House</div>
                    <img src={house3} alt="House" />
                </div>
                <div class="photo6">
                    <div className="black-box">Brick House</div>
                    <img src={house2} alt="House" />
                </div>  

                <div class="photo7">
                    <div className="black-box">Renovated</div>
                    <img src={house5} alt="House" />
                </div>
                <div class="photo8">
                    <div className="black-box">Barn House</div>
                    <img src={house4} alt="House" />
                </div>
            </div>
        </div>
    
    )
}

export default Project