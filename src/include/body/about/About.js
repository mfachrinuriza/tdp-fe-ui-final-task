import './About.css'
import '../../../assets/css/style.css'

import team1 from '../../../assets/img/team1.jpg';
import team2 from '../../../assets/img/team2.jpg';
import team3 from '../../../assets/img/team3.jpg';
import team4 from '../../../assets/img/team4.jpg';

const About = () => {
    return(
        <div>
            {/* About Section */}
            <div id="about">
                <h3>About</h3>
                <hr/>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Excepteur sint
                occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
                adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco
                laboris nisi ut aliquip ex ea commodo consequat.
                </p>
            </div>
            <div class="grid-about">
                <div class="about1">
                    <div>
                        <img src={team1} alt="Jane" />
                        <h3>Jane Doe</h3>
                        <p>CEO &amp; Founder</p>
                        <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p><button className="button-contact">Contact</button></p>
                    </div>
                </div>
                <div class="about2">
                    <div >
                        <img src={team2} alt="John" />
                        <h3>John Doe</h3>
                        <p>Architect</p>
                        <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p><button className="button-contact">Contact</button></p>
                    </div>
                </div>
                <div class="about3">
                    <div>
                        <img src={team3} alt="Mike" />
                        <h3>Mike Ross</h3>
                        <p>Architect</p>
                        <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p><button className="button-contact">Contact</button></p>
                    </div>
                </div>
                <div class="about4">
                    <div >
                        <img src={team4} alt="Dan" />
                        <h3>Dan Star</h3>
                        <p>Architect</p>
                        <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
                        <p><button className="button-contact">Contact</button></p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default About