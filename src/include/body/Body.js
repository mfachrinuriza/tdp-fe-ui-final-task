import '../../assets/css/style.css'
import './Body.css'

import About from './about/About'
import ContactList from './contact-list/ContactList'
import Contact from './contact/Contact'
import Map from './map/Map'
import Project from './project/Project'

const Body = () =>{
    return(
        <div>
            <Project/>
            <About/>
            <Contact/>
            <Map/>
            <ContactList/>
        </div>
    )
}

export default Body