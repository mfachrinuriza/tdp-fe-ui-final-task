import './ContactList.css'
import Table from './table/Table'

const ContactList = () => {
    return(
        <div>
            <div class="item17">
                <div>
                    <h3>Contact List</h3>
                    <hr/>
                    {/***  
                    add your element table here 
                    ***/}
                    <Table/>
                </div>
            </div>
            {/* End page content */}
        </div>
    )
}
export default ContactList