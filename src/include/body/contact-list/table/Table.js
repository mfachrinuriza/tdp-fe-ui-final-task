import './Table.css'
import '../../../../assets/css/style.css'

const Table = () => {
    return(
        <div className='normal-text'>
            <table class="table1">
                <tr>
                    <th width="40%">Nama</th>
                    <th width="40%">Email</th>
                    <th width="20%">Country</th>
                </tr>
                <tr>
                    <td>Alfreds F</td>
                    <td>alfreds@gmail.com</td>
                    <td>Germany</td>
                </tr>
                <tr>
                    <td>Reza</td>
                    <td>reza@gmail.com</td>
                    <td>Indonesia</td>
                </tr>
                <tr>
                    <td>Ismail</td>
                    <td>ismail@gmail.com</td>
                    <td>Turkey</td>
                </tr>
                <tr>
                    <td>Holand</td>
                    <td>holand@gmail.com</td>
                    <td>Belanda</td>
                </tr>
            </table>	
        </div>
    )
}
export default Table