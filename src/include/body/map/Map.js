import './Map.css'

import map from '../../../assets/img/map.jpg';

const Map = () =>{
    return(
        <div class="map">
            {/* Image of location/map */}
            <div>
                <img src={map} alt="maps" />
            </div>
        </div>
    )
}
export default Map