import './Navbar.css'

const Navbar = () => {
    return(
        <div className='navbar'>
            <div>
                <a href="/#"><b>EDTS</b> TDP Batch #2</a>
            </div>
            <div className='menu'>
                <a href="/#projects">Projects</a>
                <a href="/#about">About</a>
                <a href="/#contact">Contact</a>
            </div>
        </div>
    )
}
export default Navbar