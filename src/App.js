import './assets/css/style.css';
import './App.css';
import Body from 'include/body/Body';
import Footer from 'include/footer/Footer';
import Header from 'include/header/Header'
import Navbar from 'include/navbar/Navbar';



function App() {
  return (
    <div className="App">
      {/* Navbar (sit on top) */}
      <Navbar/>
      {/* Header */}
      <div className='main'>
        <Header/>
        <Body/>
      </div>
      <Footer/>
    </div>
  );
}

export default App;
